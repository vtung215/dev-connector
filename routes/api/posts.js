const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator/check');

const Post = require('../../models/Post');
const User = require('../../models/User');
const Profile = require('../../models/Profile');

// @router POST api/posts
router.post(
	'/',
	[
		auth,
		[
			check('text', 'Text is required').not().isEmpty()
		]
	],
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}

		try {
			const user = await User.findById(req.user.id).select('-password');

			const newPost = new Post ({
				text: req.body.text,
				name: user.name,
				avatar: user.avatar,
				user: req.user.id
			});

			const post = await newPost.save();

			res.status(200).send(post);
		} catch (err) {
			res.status(500).send(err.message);
		}
	}
);

// @router POST api/posts
router.get('/', async (req,res) => {
	try {
		const posts = await Post.find().sort({ date: -1 });
		res.status(200).json(posts);
	} catch (err) {
		res.status(500).send(err.message);
	}
});

// @router POST api/posts/:id
router.get('/:id', async (req,res) => {
	try {
		const post = await Post.findById(req.params.id);
		if (!post) {
			res.status(404).send({ smg: 'Post not found' });
		}
		res.status(200).json(post);
	} catch (err) {
		if (err.kind === 'ObjectId') { res.status(404).send({ smg: 'Post not found' }); }
		res.status(500).send(err.message);
	}
});

// @router delete api/posts/:id
router.delete('/:id', auth, async (req,res) => {
	try {
		const post = await Post.findById(req.params.id);

		if (post.user.toString() !== req.user.id) {
			res.json({ msg: 'User not authorized' });
		}

		await post.remove();
		res.json({ msg: 'Post deleted' });
	} catch (err) {
		res.status(500).send(err.message);
	}
});

// @router put api/posts/like/:id
router.put('/like/:id', auth, async (req,res) => {
	try {
		const post = await Post.findById(req.params.id);
		if (!post) {
			res.status(404).json({ smg: 'Post not found' });
		}

		if (post.likes.filter(like => like.user.toString() == req.user.id).length > 0) {
			res.status(400).send({ smg: 'Post already liked' });
		} else {
			post.likes.unshift({ user: req.user.id });
			await post.save();
		}

		res.status(200).json(post);
	} catch (err) {
		res.status(500).send(err.message);
	}
});

// @router delete api/posts/dislike/:id
router.delete('/dislike/:id', auth, async (req,res) => {
	try {
		const post = await Post.findById(req.params.id);
		if (!post) {
			res.status(404).json({ smg: 'Post not found' });
		}

		if (post.likes.filter(like => like.user.toString() == req.user.id).length === 0) {
			res.status(400).send({ smg: 'Post has not yet been liked' });
		} else {
			const removeIndex = post.likes.map(item => item.user).indexOf(req.user.id);
			post.likes.splice(removeIndex, 1);
			await post.save();
		}

		res.status(200).json(post);
	} catch (err) {
		res.status(500).send(err.message);
	}
});

// @router post api/posts/comment/:id
router.post(
	'/comment/:id',
	[
		auth,
		check('text', 'Text is required').not().isEmpty()
	],
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}

		try {
			const user = await User.findById(req.user.id).select('-password');
			const post = await Post.findById(req.params.id);
			if (!post) {
				res.status(404).json({ smg: 'Post not found' });
			} else {
				const newComment = {
					text: req.body.text,
					user: req.user.id,
					name: user.name,
					avatar: user.avatar
				};

				post.comments.unshift(newComment);
				await post.save();
				res.status(200).json(post);
			}
		} catch (err) {
			res.status(500).send(err.message);
		}
	}
);

// @router delete api/posts/comment/:id/:comment_id
router.delete('/comment/:id/:comment_id', auth, async (req, res) => {
	try {
		const user = await User.findById(req.user.id).select('-password');
		const post = await Post.findById(req.params.id);

		if (!post) {
			res.status(404).json({ smg: 'Post not found' });
		} else {
			const comment = post.comments.find(comment => comment.id === req.params.comment_id);
			if (!comment) {
				res.status(404).json({ smg: 'Comment not found' });
			} else {
				if (comment.user.toString() !== req.user.id) {
					return res.status(400).json({ msg: 'User not authorized' });
				} else {
					const removeIndex = post.comments.map(comment => comment.id).indexOf(req.params.comment_id);
					post.comments.splice(removeIndex, 1);
					await post.save();
					res.status(200).json(post);
				}
			}
		}
	} catch (err) {
		res.status(500).send(err.message);
	}
});

module.exports = router;
