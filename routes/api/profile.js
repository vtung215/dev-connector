const express = require('express');
const request = require('request');
const config = require('config');
const router = express.Router();
const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator/check');

const Profile = require('../../models/Profile');
const User = require('../../models/User');

// @router POST api/profile
router.post(
	'/',
	[
		auth,
		[
			check('status', 'Status is required').not().isEmpty(),
			check('skills', 'Skill is required').not().isEmpty()
		]
	],
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}

		const { company, website, location, bio, status, githubusername, skills, youtube, facebook, twitter, instagram, linkedin } = req.body;

		const profileFields = {};
		profileFields.user = req.user.id;
		if (company) profileFields.company = company;
		if (website) profileFields.website = website;
		if (location) profileFields.location = location;
		if (bio) profileFields.bio = bio;
		if (status) profileFields.status = status;
		if (githubusername) profileFields.githubusername = githubusername;
		if (skills) {
			profileFields.skills = skills.split(',').map(skill => skill.trim());
		}

		profileFields.social = {};
		if (youtube) profileFields.social.youtube = youtube;
		if (twitter) profileFields.social.twitter = twitter;
		if (facebook) profileFields.social.facebook = facebook;
		if (linkedin) profileFields.social.linkedin = linkedin;
		if (instagram) profileFields.social.instagram = instagram;

		try {
			let profile = await Profile.findOneAndUpdate(
				{ user: req.user.id },
				{ $set: profileFields },
				{ new: true, upsert: true }
			);
			res.json(profile);
		} catch (err) {
			res.status(500).send(err.message);
		}
	}
);

// @router GET api/profile
router.get('/', auth, async (req, res) => {
	try {
		const profile = await Profile.findOne({ user: req.user.id }).populate('user', ['name', 'avatar']);
		if (!profile) {
			res.status(400).json({ smg: 'There is no profile for this user' });
		}

		res.json(profile);
	} catch (err) {
		res.status(500).send(err.messgae);
	}
});

// @router GET api/profile/user/:user_id
router.get('/user/:user_id', async (req, res) => {
	try {
		const profile = await Profile.findOne({ user: req.params.user_id }).populate('user', ['name', 'avatar']);

		if (!profile) {
			res.status(400).json('Profile not found');
		}

		res.json(profile);
	} catch (err) {
		if (err.kind == 'ObjectId') {
			res.status(400).json('Profile not found');
		}
		res.status(500).send(err.message);
	}
});

// @router DELETE api/profile
router.delete('/', auth, async (req, res) => {
	try {
		await Profile.findOneAndRemove({ user: req.user.id });
		await User.findOneAndRemove({ _id: req.user.id });

		res.json({ msg: 'User deleted' });
	} catch (err) {
		res.status(500).send(err.message);
	}
});

// @router PUT api/profile/experience
router.put(
	'/experience',
	[
		auth,
		[
			check('title', 'Title is required').not().isEmpty(),
			check('company', 'Company is required').not().isEmpty()
		]
	],
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}

		const { title, company, location, from, to, current, description } = req.body;

		let newExp = { title, company, location, from, to, current, description };

		try {
			const profile = await Profile.findOne({ user: req.user.id });

			profile.experience.unshift(newExp);

			await profile.save();

			res.json(profile);
		} catch (err) {
			res.status(500).send(err.message);
		}
	}
);

// @router delete api/profile/experience/:exp_id
router.delete('/experience/:exp_id', auth, async (req, res) => {
	try {
		const profile = await Profile.findOne({ user: req.user.id }).populate('user', ['name', 'avatar']);

		const removeIndex = profile.experience.map(item => item.id).indexOf(req.params.exp_id);
		profile.experience.splice(removeIndex, 1);
		await profile.save();

		res.status(200).send(profile);
	} catch (err) {
		res.status(500).send(err.message);
	}
});

// @router PUT api/profile/education
router.put(
	'/education',
	[
		auth,
		[
			check('school', 'School is required').not().isEmpty(),
		]
	],
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}

		const { school, degree, fieldofstudy, from, to, current, description } = req.body;

		let newEducation = { school, degree, fieldofstudy, from, to, current, description };

		try {
			const profile = await Profile.findOne({ user: req.user.id });

			profile.education.unshift(newEducation);

			await profile.save();

			res.json(profile);
		} catch (err) {
			res.status(500).send(err.message);
		}
	}
);

// @router delete api/profile/experience/:exp_id
router.delete('/education/:education_id', auth, async (req, res) => {
	try {
		const profile = await Profile.findOne({ user: req.user.id }).populate('user', ['name', 'avatar']);

		const removeIndex = profile.education.map(item => item.id).indexOf(req.params.education_id);
		profile.education.splice(removeIndex, 1);
		await profile.save();

		res.status(200).send(profile);
	} catch (err) {
		res.status(500).send(err.message);
	}
});

// @router get api/github/:username
router.get('/github/:username', (req, res) => {
	try {
		const options = {
			uri: `https://api.github.com/users/${req.params.username}/repos?per_page=5&sort=created:asc&client_id=${config.get('githubClientId')}&client_secret=${config.get('githubSecret')}`,
			method: 'GET',
			headers: { 'user-agent': 'node.js' }
		};

		request(options, (error, response, body) => {
			if (error) console.log(error);

			if (response.statusCode !== 200) {
				res.status(404).json({ smg: 'No github profile found' });
			}

			res.json(JSON.parse(body));
		})
	} catch (err) {
		res.status(500).send(err.message);
	}
});

module.exports = router;
